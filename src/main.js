import axios from 'axios'
import Vue from 'vue'
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'

import Vuex from 'vuex'

import store from './store'

import filters from './utils/filters'

// Components
import App from './App.vue'
import Modal from './components/Modal'
import Footer from './Footer.vue'

// Add-ons
Vue.use(Vuex)
Vue.use(Buefy)
Vue.use(filters)

// Global Properties
Vue.prototype.$http = axios

// Filters
// Vue.filter('euros', (val) => {
//   // FROM: https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/NumberFormat
//   return new Intl.NumberFormat('fr-FR', {
//     style: 'currency',
//     currency: 'EUR'
//   }).format(val)
// })

// Vue.filter('humanDate', (val) => {
//   let year = val.getFullYear()
//   let month = val.getMonth() +1
//   let day = val.getDate()
//   day = (day < 10) ? '0' + day : day
//   month = (month < 10) ? '0' + month : month
//   return `${day}/${month}/${year}`
// })

// Vue.filter('capitalize', (val) => {
//   return val.replace(/(^|\s)\S/g, l => l.toUpperCase())
// })
 
new Vue({
  el: '#app',
  store,
  template: `
  <div>
    <App/>
    <Footer/>
    <Modal/>
  </div>`,
  components: {
    App,
    Modal,
    Footer
  }
})
