export const shuffle = (array) => {
  // FROM: https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array#answer-6274381
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

export const shuffleArray = (o) => {
  for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x)
  return o
}

export const debounce = (callback, delay) => {
  let timer
  return () => {
    let args = arguments
    let context = this
    clearTimeout(timer)
    timer = setTimeout(() => {
      callback.apply(context, args)
    }, delay)
  }
}

export const isMobile = {
  updated() {
    this.mobileBrowser()
  },
  methods: {
    mobileBrowser() {
      const devices = ['Android', 'webOS', 'iPhone', 'iPad', 'iPod', 'BlackBerry', 'Windows Phone']
      for (let i = 0; i < devices.length; i++) {
        if (navigator.userAgent.match(/devices[i]/i)) {
          return true
        }
      }
      return false
    }
  }
}