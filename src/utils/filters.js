/*
* TECHNIQUE TO IMPORT EACH FILTER GLOBALLY IN VUEJS 
* FROM: https://stackoverflow.com/questions/47004702/how-to-add-a-bunch-of-global-filters-in-vue-js#answer-47005707
*/

const capitalize = val => {
  val.replace(/\b\w/g, l => l.toUpperCase())
}

const humanDate = val => {
  let year = val.getFullYear(),
      month = val.getMonth() + 1,
      day = val.getDate()
  day = (day < 10) ? '0' + day : day
  month = (month < 10) ? '0' + month : month
  return `${day}/${month}/${year}`
}

const euros = val => {
  // FROM: https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/NumberFormat
  return new Intl.NumberFormat('fr-FR', {
    style: 'currency',
    currency: 'EUR'
  }).format(val)
}

const shuffle = values => {
  for (let i = values.length; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1)),
        temp = values[i]
    
    values[i] = values[j]
    values[j] = temp  
  }
  return values
}

const length = arr_values => ( arr_values.length > 0 ) ? arr_values.length : '' 

const tighten = (str, depth = 10) => {
  const cut = str.split(' ')
  if (cut.length <= 1 || depth > cut.length)
    return str
  
  const nu_str = cut.slice(0, depth)
  
    return nu_str.join(' ') + '...'
}

export default {
  install(Vue) {
    Vue.filter('capitalize', capitalize),
    Vue.filter('humanDate', humanDate),
    Vue.filter('euros', euros),
    Vue.filter('length', length),
    Vue.filter('shuffle', shuffle),
    Vue.filter('tighten', tighten)
  }
}