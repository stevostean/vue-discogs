export const state = {
  cards: [],
  records: [],
  activeModal: null,
  tmp: null,
  last: null,
  navigationView: 'Card',
  filteredRecords: null,
  genres: null
}