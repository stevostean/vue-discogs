export const actions = {
  getFilteredRecords({commit}, obj) {
    commit(obj[0], obj[1])
  },
  getSearchRecords({commit}, term) {
    commit('updateSearchRecords', term)
  },
  resetAnySearch({commit}) {
    commit('resetRecordList')
  },
  defineGenres({commit}, obj) {
    commit('setGenres', obj)
  },
  displayRecordsByGenre({commit}, obj) {
    commit('getRecordsByGenre', obj)
  }
}