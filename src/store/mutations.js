export const mutations = {
  // Records
  dumpAllRecords(state, recordList) {
    let recordArray = recordList
    for (let i = 0; i < recordArray.length; i++) {
      recordArray[i].row_id = i + 1
      
    }
    state.records = recordArray
    return
  },
  // Card Cache System
  addCardToCache(state, cardData) {
    for (let i = 0; i < state.cards.length; i++) {
      const element = state.cards[i];
      if (element.release_id === cardData.release_id) {
        return element
      }
    }
    state.cards.push(cardData)
    state.last = cardData
    return cardData
  },
  // Modal
  modalOn(state, cardId) {
    state.tmp = cardId
    state.activeModal = 'is-active'
  },
  modalOff(state) {
    state.activeModal = null
  },
  // Navigation
  updateNavigationView(state, payload) {
    state.navigationView = payload
  },
  setGenres(state, payload) {
    // filter object: if key as empty list, then delete it
    for (const key in payload) {
      let value = payload[key]
      if (value === null || value === undefined || value.length < 1)
        delete payload[key]
    }

    payload['aucune'] = []
    // ordering final object
    state.genres = Object.keys(payload).sort().reduce((result, key) => {
      result[key] = payload[key]
      return result
    }, {})
  },
  updatePriceFilter(state, payload) {
    state.priceRange = payload
    const cmd = payload[0]
    const value = payload[1]
    this.updateRecordList([cmd, value])
  },
  updateRecordsLess(state, payload) {
    if (!payload) {
      state.filteredRecords = null
      return
    }
    const value = parseFloat(payload)
    const recs = state.records.filter((curr) => {
      return curr.price < value
    })
    state.filteredRecords = recs
  },
  updateRecordsMore(state, payload) {
    if (!payload) {
      state.filteredRecords = null
      return
    }
    const value = parseFloat(payload)
    const recs = state.records.filter((curr) => {
      return curr.price > value
    })
    state.filteredRecords = recs
  },
  updateRecordsEqual(state, payload) {
    if (!payload) {
      state.filteredRecords = null
      return
    }
    const value = parseFloat(payload)
    const recs = state.records.filter((curr) => {
      return curr.price === value
    })
      
    state.filteredRecords = recs
  },
  updateSearchRecords(state, payload) {
    if (!payload) {
      state.filteredRecords = null
      return
    }
    const artists = state.records.filter((curr) => {
      let current = curr.artist,
          re = new RegExp(payload, 'i')

      if (re.test(current)) {
        return current
      }
      return 
    })
    const titles = state.records.filter((curr) => {
      let current = curr.title,
          re = new RegExp(payload, 'i')

      if (re.test(current)) {
        return current
      }
      return 
    })

    // Managing uniqueness of two merged lists, in a one line solution
    let mergedLists = artists.concat(titles.filter((curr) => {
      return artists.indexOf(curr) < 0
    }))

    state.filteredRecords = mergedLists
  },
  getRecordsByGenre(state, payload) {
    // get list of release_id and convert to Int
    const releases = state.genres[payload].map((curr) => parseInt(curr))

    let recs = []
    for (let i = 0; i < releases.length; i++) {
      recs.push(state.records.find(x => x.release_id === releases[i]))
    }
    
    state.filteredRecords = recs
  },
  resetRecordList(state) {
    state.filteredRecords = null
  },
  updateRecordsOnStyles(state, payload) {
    const recs = state.records.filter((curr) => {
      return curr.genre === payload.toLowerCase()
    })

    state.filteredRecords = recs
  }
}