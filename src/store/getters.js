import { totalmem } from "os";

// just a small utility, to avoid repetition...
const totalPrice = (st) => {
  let prices = []
  let Rec = st.records
  for (let i = 0; i < Rec.length; i++) {
    prices.push(Rec[i].price)
  }
  const final = prices.reduce((a,b) => {
    return a + b
  }, 0)
  return final
}

export const getters = {
  cardInCache: (state) => (id) => {
    return state.cards.find(card => card.release_id === id)
  },
  getTotalPrice(state) {
    return totalPrice(state)
  },
  getTotalAmount(state) {
    return state.records.length
  },
  getAverage(state) {
    return totalPrice(state) / state.records.length
  },
  getMedianPrice(state) {
    const entries = state.records.length,
                    half = Math.floor(entries / 2)

    let prices = []
    for (let i = 0; i < entries; i++) {
      prices.push(state.records[i].price)      
    }

    if (entries % 2)
      return (prices[half - 1] + prices[half]) / 2.0
    else
      return prices[half]
  },
  renderRecordsList(state) {
    if (state.filteredRecords instanceof Array) {
      return state.filteredRecords
    }
    return state.records
  }  
}