# Vue Discogs

> My Discogs Marketplace as a SPA with Vue.js

### Build Setup

``` bash
# install dependencies
npm install
yarn install

# serve with hot reload at localhost:8080
npm run dev
yarn dev

# build for production with minification
npm run build
yarn build
```

### Interface functionalities

- Radio switch buttons, to select Card/List View.
- Input to search records by price with operand (dropdown menu) - *see below*
- Search input to find records by *artists or title:* **ALT+S** to get input and focus - *see below*

### KEYBINDINGS

There's 2 of them:

- **ALT + W:** display in the header more details about the whole collection
- **ALT + S:** display in the header search fields to find anything (sort by
  price, genres submenu, search artists/titles by string)
